import java.util.Scanner;
public class MyApp{
  public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Ketik nama, email, dan no. HP: ");

    String response = scanner.nextLine();
    String[] responseArray = response.split(" ");

    if(responseArray.length == 3) {
      Student student = new Student(responseArray[0], responseArray[1], responseArray[2]);
      System.out.println("Student: " + student);    
      System.out.println("Student.getName(): " + student.getName());
      System.out.println("Student.getEmail(): " + student.getEmail());
      System.out.println("Student.getPhoneNumber(): " + student.getPhoneNumber());

    }else {
      System.out.print("Missing/extra parameters!");
    }

    scanner.close();
  }
}

